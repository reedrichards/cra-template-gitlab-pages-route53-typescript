# cra-template-gitlab-pages-route53-typescript

This is the official TypeScript template for [Create React App](https://github.com/facebook/create-react-app), modified to include a ci/cd pipeline on gitlab to deploy the page to gitlab pages and create a domain name entry in route 53. 

To use this template, add `--template gitlab-pages-route53-typescript` when creating a new app.

For example:

```sh
npx create-react-app my-app --template gitlab-pages-route53-typescript

# or

yarn create react-app my-app --template gitlab-pages-route53-typescript

# or if cloned locally 

npx create-react-app my-app --template file:./cra-template-gitlab-pages-route53-typescript
```

For more information, please refer to:

- [Getting Started](https://create-react-app.dev/docs/getting-started) – How to create a new app.
- [User Guide](https://create-react-app.dev) – How to develop apps bootstrapped with Create React App.
